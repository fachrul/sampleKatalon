Hii there,
Mostly i do scripting in Katalon, and I feel auto formatting source code in script mode is not  convenient, sometimes it automatically add new line at keyword call, the println "something" became println("something"), i prefer less parentheses (groovy goodness), and sometimes i got my method parameters argument is empty, this unexpected behavior makes my script very fragile
Is there any way in Katalon preference to disable auto formatting in script like this?
I'm hardy find the setting by myself :(

![Bug](script-changed-when-manual-input.gif "Bug")

Step to reproduce:
* Coding in `script` mode
* Add method in `manual` mode

What changed?
* script have parentheses, like `println "some string"` become `println("some string")`
* arguments in method call disappears
* added new line every statement